# Etapes TDD cas réel

## Point de départ
Récupération des règles de gestion, qui décrivent la fonctionnalité à implémenter

## validation RG01
jest validation-rg01

## Ajout d'une règle de gestion RG02
jest validation-rg02
