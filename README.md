# Meetup TDD : tdd-jest

## TDD : Test Driven Developement

"Fail Fast, Fail Often"

=== 3 Règles ===

1. Pas de production de code avant d'avoir écrit au moins un test en erreur
2. Dès qu'un test en erreur a été ajouté, il faut produire le code qui passe le test
3. Pas plus de production de code, que ce qu'il est nécessaire pour passer le test en erreur

## Le test unitaire
- rapide à exécuter (very fast to execute)
- facile d'utilisation (relatively easy to setup, not requiring any elaborate configuration)
- résultat précis (very precise in the feedback they provide)

Les tests unitaires restent limités car ils ne sont pas end-to-end.
Chaque test ne teste qu'une portion de code.
Ils sont donc à utiliser en complément d'autres tests (intégration, fonctionnels..).


## Jest

Jest a été créé par Facebook, dans le but de tester des applications React.
Par la suite, l'utilisation de Jest s'est étendue à tout l'environnement js front et back: nodejs, angular, vuejs...

Jest est un framework qui intègre un outil en ligne de commande (CLI).
Quelques particularités de Jest:
- zero configuration
- isolation (des tests entre eux)
- riche: beaucoup d'assertions sont fournies dans la librairie
- outils utiles: coverage, filtres, mocking, snapshots