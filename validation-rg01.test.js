const isValide = require('./validation-main.js');

// Test 1
// test('valider la longueur d\'une adresse email ok', () => {
//     expect(isValide("email@test.com")).toBe(true);
// })

// Test 2
// test('valider la longueur d\'une adresse email ko', () => {
//     expect(isValide("lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-integer-pellentesque-odio-leo-vel-accumsan-orci-pretium-at-duis-vel-risus-felis-etiam-eu-accumsan-felis-fusce-at-odio-ultrices-sollicitudin-lacus-quis-laoreet-metusmaecenas-fringilla-lorem-vel-enim-pretium-posuere-aliquam-vitae-dui-sollicitudin-molestie-dolor-ac-tristique-arcu@test.com")).toBe(false);
// })