Le code retourne "valide" si l'adresse est conforme, et "invalide" sinon.

<u>RG01: Règle 1</u>: 

La taille de l'email ne doit pas dépasser 320 caractères.

<u>RG02: Règle 2</u>: 

- La partie avant l'@ contient des lettres (min ou maj), des chiffres, '.', '-', ou '_', et comporte au moins 2 caractères
- La partie entre l'@ et le dernier point contient des lettres (min ou maj), ou des '-', et comporte au moins 2 caractères
- La partie après le dernier point contient des lettres (min ou maj), et comporte entre 2 et 4 caractères
