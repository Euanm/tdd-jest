const isValide = require('./validation-main.js');

// locale
// test('valider une adresse email ok', () => {
//     expect(isValide("email@test.com")).toBe(true);
// })

// test('valider une adresse email : locale ko', () => {
//     expect(isValide("em$ail@test.com")).toBe(false);
// })

// test('valider une adresse email : locale vide ko', () => {
//     expect(isValide("@test.com")).toBe(false);
// })

// domaine
// test('valider une adresse email : domaine ok', () => {
//     expect(isValide("email@te_st.com")).toBe(true);
// })

// test('valider une adresse email : domaine ko', () => {
//     expect(isValide("email@test123$.com")).toBe(false);
// })

// extension
// test('valider une adresse email : extension ko', () => {
//     expect(isValide("email@test.extension")).toBe(false);
// })