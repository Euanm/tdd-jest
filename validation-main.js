const rg01 = require('./validation-rg01');
const rg02 = require('./validation-rg02');

function isValide(input) {
    // rg01
    return rg01(input);
    // rg02
    // return rg01(input) && rg02(input);
}

module.exports = isValide;